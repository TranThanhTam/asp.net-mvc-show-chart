﻿using Chart_Dashborad.Models;
using Microsoft.EntityFrameworkCore;

namespace Chart_Dashborad.DataLayer
{
	public class DbContextSale : DbContext
	{
		public DbContextSale(DbContextOptions options) : base(options)
		{
		}

		public DbSet<SaleEnitity> Sales { get; set; }
	}
}
