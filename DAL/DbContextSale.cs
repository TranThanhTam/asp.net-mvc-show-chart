﻿using Microsoft.EntityFrameworkCore;

namespace DAL
{
	public class DbContextSale : DbContext
	{
		public DbContextSale(DbContextOptions options) : base(options)
		{

		}

		public DbSet<SaleEnitity> Sales { get; set; }
	}
}